//
//  SelectLanguageViewController.swift
//  Yummy-Project
//
//  Created by WASIQ-MACBOOK on 30/01/2021.
//

import UIKit

class SelectLanguageViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    //MARK:- OUTLET
    @IBOutlet weak var tableView0: UITableView!
    
    //MARK:- VARIABLE DECLARATION
    
    
    var arrLanguage = ["English","Urdu","Punjabi"]
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView0.tableFooterView = UIView()
    }
    //MARK:- VIEW WILL APPEAR
    
    override func viewWillAppear(_ animated: Bool) {
        if  AppUtility?.getObject(forKey: "DarKMode") == "ON"{
            overrideUserInterfaceStyle = .dark
            
        }else{
            overrideUserInterfaceStyle = .light
            
        }
    }
    //MARK:- BUTTON ACTION
    
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TABLEVIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLanguage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectLanguageTableViewCell", for: indexPath)as! SelectLanguageTableViewCell
        cell.lblName.text =  String(self.arrLanguage[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) {
            if  AppUtility?.getObject(forKey: "DarKMode") == "ON"{
                overrideUserInterfaceStyle = .dark
                cell.tintColor = UIColor.white
                cell.accessoryType = .checkmark
                
                
            }else{
                overrideUserInterfaceStyle = .light
                cell.tintColor = UIColor.systemBlue
                cell.accessoryType = .checkmark
                
            }
        }
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            
            cell.accessoryType = .none
            
        }
    }
    
    
    
    
    
}
