//
//  settingViewController.swift
//  Yummy-Project
//
//  Created by WASIQ-MACBOOK on 29/01/2021.
//

import UIKit

class settingViewController: UIViewController {
    //MARK:- OUTLET
    
    @IBOutlet weak var switchDarkMode: UISwitch!
    
    @IBOutlet weak var tfContact: UILabel!
    @IBOutlet weak var tfLanguage: UILabel!
    //MARK:- VARIABLE DECLARATION
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    //MARK:- VIEW WILL APPEAR
    
    override func viewWillAppear(_ animated: Bool) {
       
        
        if  AppUtility?.getObject(forKey: "DarKMode") == "ON"{
    
            overrideUserInterfaceStyle = .dark
            self.switchDarkMode.isOn =  true
            UIApplication.shared.statusBarStyle = .lightContent
            setNeedsStatusBarAppearanceUpdate()
            
            
        }else{
            
            overrideUserInterfaceStyle = .light
            self.switchDarkMode.isOn =  false
            UIApplication.shared.statusBarStyle = .darkContent
            setNeedsStatusBarAppearanceUpdate()
    
        }
    }
    
    //MARK:- FUNCTION
    
    //MARK:- BUTTON ACTION
    
    @IBAction func changePasswordButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController")as! ChangePasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func privacyButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func termButtonPressed(_ sender: Any) {
    }
    
    @IBAction func changeButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SelectLanguageViewController")as! SelectLanguageViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    
    //MARK:- SWITCH ACTION
    
    @IBAction func switchDarkModePressed(_ sender: UISwitch) {
        
        if switchDarkMode.isOn == true{
            
            self.overrideUserInterfaceStyle = .dark
            AppUtility?.saveObject(obj: "ON", forKey: "DarKMode")
            NotificationCenter.default.post(name: Notification.Name("darkmodeNoti"), object: nil)
            UIApplication.shared.statusBarStyle = .lightContent
            setNeedsStatusBarAppearanceUpdate()
            
        
            
        }else{
            
            self.overrideUserInterfaceStyle = .light
            AppUtility?.saveObject(obj: "OFF", forKey: "DarKMode")
            NotificationCenter.default.post(name: Notification.Name("darkmodeNoti"), object: nil)
            UIApplication.shared.statusBarStyle = .darkContent
            setNeedsStatusBarAppearanceUpdate()
            
            
        }
    }
    
    
}
