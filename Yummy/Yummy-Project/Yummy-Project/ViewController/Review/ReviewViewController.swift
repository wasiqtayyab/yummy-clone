//
//  ReviewViewController.swift
//  Yummy-Project
//
//  Created by WASIQ-MACBOOK on 30/01/2021.
//

import UIKit

class ReviewViewController: UIViewController {
    
    //MARK:- OUTLET
    @IBOutlet weak var tvReview: UITextView!
    
    
    //MARK:- VARIABLE DECLARATION
    
    
    
    //MARK:- VIEW DID LOAD
  override func viewDidLoad() {
        super.viewDidLoad()

    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        if  AppUtility?.getObject(forKey: "DarKMode") == "ON"{
            overrideUserInterfaceStyle = .dark
            UIApplication.shared.statusBarStyle = .lightContent
            setNeedsStatusBarAppearanceUpdate()
            
        }else{
            overrideUserInterfaceStyle = .light
            UIApplication.shared.statusBarStyle = .darkContent
            setNeedsStatusBarAppearanceUpdate()
            
        }
    }

    //MARK:- FUNCTION
    
    //MARK:- BUTTON ACTION

    @IBAction func submitButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "RecipeSaveViewController")as! RecipeSaveViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
    }
    
    
}
