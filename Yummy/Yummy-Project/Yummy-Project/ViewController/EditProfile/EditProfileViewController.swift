//
//  EditProfileViewController.swift
//  Yummy-Project
//
//  Created by WASIQ-MACBOOK on 29/01/2021.
//

import UIKit

class EditProfileViewController: UIViewController,UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var imagePicker: UIButton!
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    
    @IBOutlet weak var tfEmail: UITextField!
    
    @IBOutlet weak var tfCountry: UITextField!
    
    @IBOutlet weak var tfDOB: UITextField!
    
    
    //MARK:- VARIABLE DECLARATION
    
    var datePicker = UIDatePicker()
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        showDatePickerTo()
        
    }
    //MARK:- VIEW WILL APPEAR
    
    
    
    //MARK:- FUNCTION
    
    func setup(){
        
        if #available(iOS 13.4, *) { datePicker.preferredDatePickerStyle = .wheels
            
        }
        self.tfDOB.delegate = self
        
    }
    
    //MARK:- Date Picker
    func showDatePickerTo(){
        
        datePicker.datePickerMode = .date
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePickerTo));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePickerTo));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        self.tfDOB.inputAccessoryView = toolbar
        self.tfDOB.inputView = datePicker
        
    }
    
    @objc func donedatePickerTo(){
        
        let formatter0 = DateFormatter()
        formatter0.dateFormat = "MMM yyyy"
      
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "dd/MM/yyyy"
        let FullDate = "\(formatter1.string(from: datePicker.date))"
        let ObjDate = FullDate.components(separatedBy: "/")
     
        //Current Date
        
        let yesterday = Calendar.current.date(byAdding: .day, value: 0, to: Date())
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd/MM/yyyy"
        let myString = dateFormatterPrint.string(from: yesterday! as Date)

        print("Date",myString) // Feb 01,2018
        
        let ObjCurrntDate = myString.components(separatedBy: "/")
        let day     = ObjCurrntDate[0]
        let month  = ObjCurrntDate[1]
        let Year   = ObjCurrntDate[2]
               
       
        if ObjDate[2] ==  Year{
            if ObjDate[1] == month {
                if ObjDate[0] == day  {
                    print("Same Day Select")
                }else{
                    if ObjDate[0] > day  {
                        
                        print("Select Correct day")
                        
                        AppUtility?.displayAlert(title: NSLocalizedString("alert_app_name", comment: ""), messageText:"Please select day less or equal to \(myString)", delegate: self)
                        return
                    }
                }
            }else{
                if ObjDate[1] > month  {
                    
                    print("Select Correct month")
                    
                    AppUtility?.displayAlert(title: NSLocalizedString("alert_app_name", comment: ""), messageText:"Please select month less or equal to \(myString)", delegate: self)
                    return
                }
            }
        }else{
            if ObjDate[2] > Year  {
                
                print("Select Correct year")
                
                AppUtility?.displayAlert(title: NSLocalizedString("alert_app_name", comment: ""), messageText:"Please select year less or equal to \(myString)", delegate: self)
                return
            }
        }
        
        self.tfDOB.text! = "\(formatter0.string(from: datePicker.date))"
        print("Date Of Registration : \(self.tfDOB.text!)")
        self.view.endEditing(true)
        
    }
    
    @objc func cancelDatePickerTo(){
        self.view.endEditing(true)
    }

    
    //MARK:- BUTTON ACTION
  
    @IBAction func editProfileButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func menuBarButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func editUsernameButtonPressed(_ sender: UIButton) {
    }
    
    
}
