//
//  BmiCalculatorViewController.swift
//  Yummy-Project
//
//  Created by WASIQ-MACBOOK on 30/01/2021.
//

import UIKit

class BmiCalculatorViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    //MARK:- OUTLET
    
    @IBOutlet weak var lblCalculator: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var lblHealth: UILabel!
    @IBOutlet weak var lblBmi: UILabel!
    
    @IBOutlet weak var lblResult: UILabel!
    
    @IBOutlet weak var tfAge: UITextField!
    @IBOutlet weak var tfGender: UITextField!
    @IBOutlet weak var tfHeight: UITextField!
    @IBOutlet weak var tfWeight: UITextField!
    
    @IBOutlet weak var btnBack: UIButton!
    //MARK:- VARIABLE DECLARATION
    var selectedGender: String?
    var selectedHeight: String?
    var selectedWeight: String?
    
    var weightList: [Int] = Array(40...100)
    var heightList: [Int] = Array(152...218)
    var genderList = ["Unspecified","Male", "Female"]
    
    
    var weight : Double?
    var height : Double?
    var bmiResult : Int?
    
    
    
    var genderpickerView = UIPickerView()
    var heightpickerView = UIPickerView()
    var weightpickerView = UIPickerView()
    
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tfAge.delegate = self
        setup()
    }
    
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        if  AppUtility?.getObject(forKey: "DarKMode") == "ON"{
            overrideUserInterfaceStyle = .dark
            UIApplication.shared.statusBarStyle = .lightContent
            setNeedsStatusBarAppearanceUpdate()
            
        }else{
            overrideUserInterfaceStyle = .light
            UIApplication.shared.statusBarStyle = .darkContent
            setNeedsStatusBarAppearanceUpdate()
            
        }
    }
    
    
    //MARK:- FUNCTION
    
    func setup(){
        lblResult.isHidden = true
        lblCalculator.isHidden = true
        createPickerView()
        dismissPickerView()
        btnBack.isHidden = true
        
    }
    
    func backButtonChanges(){
        
        lblHealth.text = "HEALTH APP"
        lblBmi.text = "CALCULATE YOUR BMI"
        btnMenu.isHidden = false
        btnBack.isHidden = true
        lblResult.isHidden = true

        
    }
    
    func formulaCalculation(){
        lblBmi.text = nil
        //height in metres
        print(height)
        height = Double(height! / 100)
        print(height!)
        
        bmiResult = Int((weight! / (height! * height!)))
       
        print(bmiResult)
        btnBack.isHidden = false
        btnMenu.isHidden = true
        lblCalculator.isHidden = false
        lblHealth.text = "YOUR RESULT"
        lblBmi.text = String(bmiResult!)
        
        lblResult.isHidden = false
        
    }
    
    //MARK:- TEXT FIELD DELAGATE
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool {
        if textField == tfAge {
            let maxLength = 3
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        backButtonChanges()
    }
    
    @IBAction func calculateButtonPressed(_ sender: UIButton) {
        
        formulaCalculation()
      
    }
    
    @IBAction func menuButtonPressed(_ sender: UIButton) {
        
    }
    
    //MARK:- UIPICKER VIEW
    
    func createPickerView() {
        
        genderpickerView.delegate = self
        heightpickerView.delegate = self
        weightpickerView.delegate = self
        tfGender.inputView = genderpickerView
        tfHeight.inputView = heightpickerView
        tfWeight.inputView = weightpickerView
    
        
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        tfGender.inputAccessoryView = toolBar
        tfHeight.inputAccessoryView = toolBar
        tfWeight.inputAccessoryView = toolBar
    }
    @objc func action() {
        view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerView == genderpickerView{
            return 1
        }else if pickerView == heightpickerView {
            return 1
        }else{
            return 1
        }
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        if pickerView == genderpickerView{
            return self.genderList.count
        }else if pickerView == heightpickerView {
            return self.heightList.count
        }else{
            return self.weightList.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == genderpickerView{
            return genderList[row]
        }else if pickerView == heightpickerView {
            return "\(heightList[row])"
        }else{
            return "\(weightList[row])"
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == genderpickerView{
            selectedGender = genderList[row]
            tfGender.text = selectedGender
            
        }else if pickerView == heightpickerView {
            selectedHeight = "\(heightList[row])"
            tfHeight.text = "\(selectedHeight!) cm"
            
             height = Double(selectedHeight!)
            print("\(height!)")
            
        }else{
            selectedWeight = "\(weightList[row])"
            
            tfWeight.text = "\(selectedWeight!) kg"
             weight = Double(selectedWeight!)
            print("\(weight!)")
            
        }
        
    }
    
}
