//
//  ForgetPasswordViewController.swift
//  Yummy-Project
//
//  Created by WASIQ-MACBOOK on 29/01/2021.
//

import UIKit

class ForgetPasswordViewController: UIViewController {
    
    //MARK:- OUTLET
    
    
    @IBOutlet weak var tfEmail: UITextField!
    
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        if  AppUtility?.getObject(forKey: "DarKMode") == "ON"{
            overrideUserInterfaceStyle = .dark
            UIApplication.shared.statusBarStyle = .lightContent
            setNeedsStatusBarAppearanceUpdate()
            
        }else{
            overrideUserInterfaceStyle = .light
            UIApplication.shared.statusBarStyle = .darkContent
            setNeedsStatusBarAppearanceUpdate()
            
        }
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func resetButtonPressed(_ sender: UIButton) {
        
        guard AppUtility?.isEmail(tfEmail.text!) == true else {
            
            AppUtility?.displayAlert(title: "Invalid Email", messageText: "Email is not Valid", delegate: self)
            
            
            return
            
        }
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "EmailOtpViewController")as! EmailOtpViewController
        vc.strEmail = self.tfEmail.text!
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    

}
