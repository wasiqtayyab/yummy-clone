//
//  EmailOtpViewController.swift
//  Yummy-Project
//
//  Created by WASIQ-MACBOOK on 29/01/2021.
//

import UIKit

class EmailOtpViewController: UIViewController,UITextFieldDelegate {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfOTP1: UITextField!
    @IBOutlet weak var tfOTP2: UITextField!
    @IBOutlet weak var tfOTP3: UITextField!
    @IBOutlet weak var tfOTP4: UITextField!
    
    
    @IBOutlet weak var otp1View: UIView!
    @IBOutlet weak var otp2View: UIView!
    @IBOutlet weak var otp3View: UIView!
    @IBOutlet weak var otp4View: UIView!
    
    
    @IBOutlet weak var lblEmail: UILabel!
    
    
    //MARK:- DECLERATION
    
    var strEmail = ""
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        if  AppUtility?.getObject(forKey: "DarKMode") == "ON"{
            overrideUserInterfaceStyle = .dark
            UIApplication.shared.statusBarStyle = .lightContent
            setNeedsStatusBarAppearanceUpdate()
            
        }else{
            overrideUserInterfaceStyle = .light
            UIApplication.shared.statusBarStyle = .darkContent
            setNeedsStatusBarAppearanceUpdate()
            
        }
    }
    //MARK:- FUNCTION
    
    func setup(){
        
        self.tfOTP1.delegate = self
        self.tfOTP2.delegate = self
        self.tfOTP3.delegate = self
        self.tfOTP4.delegate = self
        self.lblEmail.text = "We've sent you verification code at \(strEmail)"
        
        otp1View.layer.borderColor = #colorLiteral(red: 0.7607843137, green: 0.7607843137, blue: 0.7607843137, alpha: 0.95)
        otp1View.layer.borderWidth = 0.5
        otp1View.layer.cornerRadius = 3
        
        otp2View.layer.borderColor = #colorLiteral(red: 0.7607843137, green: 0.7607843137, blue: 0.7607843137, alpha: 0.95)
        otp2View.layer.borderWidth = 0.5
        otp2View.layer.cornerRadius = 3
        
        otp3View.layer.borderColor = #colorLiteral(red: 0.7607843137, green: 0.7607843137, blue: 0.7607843137, alpha: 0.95)
        otp3View.layer.borderWidth = 0.5
        otp3View.layer.cornerRadius = 3
        
        otp4View.layer.borderColor = #colorLiteral(red: 0.7607843137, green: 0.7607843137, blue: 0.7607843137, alpha: 0.95)
        otp4View.layer.borderWidth = 0.5
        otp4View.layer.cornerRadius = 3
        
        
        
    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func verifyCodeButtonPressed(_ sender: UIButton) {
        
        if(tfOTP1.text?.isEmpty)!{
            
            AppUtility?.displayAlert(title: NSLocalizedString("Verification Code", comment: "") , messageText: "Please enter your Verification Code.", delegate: self)
        }
        if(tfOTP2.text?.isEmpty)!{
            
            AppUtility?.displayAlert(title: NSLocalizedString("Verification Code", comment: "") , messageText: "Please enter your Verification Code.", delegate: self)
        }
        if(tfOTP3.text?.isEmpty)!{
            
            AppUtility?.displayAlert(title: NSLocalizedString("Verification Code", comment: "") , messageText: "Please enter your Verification Code.", delegate: self)
        }
        
        if(tfOTP4.text?.isEmpty)!{
            
            AppUtility?.displayAlert(title: NSLocalizedString("Verification Code", comment: "") , messageText: "Please enter your Verification Code.", delegate: self)
        }
        
        
        
       
        
    }
    
    
    //MARK:- TEXTFIELD DELEGATE
  
    @IBAction func textEditChanged(_ sender: UITextField) {
            print("textEditChanged has been pressed")
            let count = sender.text?.count
            //
            if count == 1{
                
                switch sender {
                case tfOTP1:
                    tfOTP2.becomeFirstResponder()
                case tfOTP2:
                    tfOTP3.becomeFirstResponder()
                case tfOTP3:
                    tfOTP4.becomeFirstResponder()
                case tfOTP4:
                    tfOTP4.becomeFirstResponder()
                    
               
                default:
                    print("default")
                }
            }
            
        }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            textField.text = ""
            if textField.text == "" {
                print("Backspace has been pressed")
            }
            
            if string == ""
            {
                print("Backspace was pressed")
                switch textField {
                case tfOTP2 :
                    tfOTP1.becomeFirstResponder()
                case tfOTP3:
                    tfOTP2.becomeFirstResponder()
                case tfOTP4:
                    tfOTP3.becomeFirstResponder()
                 
                default:
                    print("default")
                }
                textField.text = ""
                return false
            }
            
            return true
        }
    


}
