//
//  ViewController.swift
//  Yummy-Project
//
//  Created by WASIQ-MACBOOK on 28/01/2021.
//

import UIKit
import GoogleSignIn
import AuthenticationServices
import FBSDKLoginKit

class LoginViewController: UIViewController,GIDSignInDelegate, ASAuthorizationControllerDelegate {
    
    //MARK:- OUTLETS
    
    @IBOutlet weak var tfEmail: UITextField!
    
    @IBOutlet weak var tfPassword: UITextField!
    
    @IBOutlet weak var btnEye: UIButton!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    
    @IBOutlet weak var btnApple: UIButton!
    
    @IBOutlet weak var btnGoogle: UIButton!
    
    @IBOutlet weak var btnFacebook: UIButton!
    
    //MARK:- VRIABLE DECLERATION
    
    var isSecure = true
    
    
    var sEmail = ""
    var sFname = ""
    var sLname = ""
    var sUsername = ""
    var sID = ""
    
    
    
    
    
    //MARK:- VIEW WILL APPEAR
    ///Dark mode
    override func viewWillAppear(_ animated: Bool) {
        if  AppUtility?.getObject(forKey: "DarKMode") == "ON"{
            overrideUserInterfaceStyle = .dark
            UIApplication.shared.statusBarStyle = .lightContent
            setNeedsStatusBarAppearanceUpdate()
            
        }else{
            overrideUserInterfaceStyle = .light
            UIApplication.shared.statusBarStyle = .darkContent
            setNeedsStatusBarAppearanceUpdate()
            
        }
    }
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        
    }
    
    
    
    //MARK:-BUTTON ACTION
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        guard AppUtility?.isEmail(tfEmail.text!) == true else{
            AppUtility?.displayAlert(title: "Invalid Email", messageText: "Email is not Valid", delegate: self)
            return
            
        }
        
    }
    
    @IBAction func appleButtonPressed(_ sender: UIButton) {
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
        
        
    }
    
    @IBAction func googleButtonPressed(_ sender: UIButton) {
        GIDSignIn.sharedInstance()?.signIn()
        
    }
    
    @IBAction func facebookButtonPressed(_ sender: UIButton) {
        loginfb()
    }
    
    @IBAction func signupButtonPressed(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "SignUpViewController")as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func eyeButtonPressed(_ sender: UIButton) {
        
        if self.isSecure == true {
            self.tfPassword.isSecureTextEntry =  false
            self.btnEye.setImage(UIImage(named: "openeye"), for: .normal)
            self.isSecure =  false
        }else{
            self.tfPassword.isSecureTextEntry =  true
            self.btnEye.setImage(UIImage(named: "eye"), for: .normal)
            self.isSecure =  true
        }
        
    }
    
    
    @IBAction func forgetPasswordButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordViewController")as! ForgetPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:- GOOGLE AUTHETICATION
    
    
    func sign(_ signIn: GIDSignIn!,
              didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        
        // Check for sign in error
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let firstName = user.profile.givenName
        let lastName = user.profile.familyName
        let email = user.profile.email
        
        
        sEmail = email!
        sFname = firstName!
        sLname = lastName!
        sUsername = fullName!
        sID = idToken!
        
        print("email",sEmail)
        print("firstName",sFname)
        print("secondName",sLname)
        print("username",sUsername)
        print("idToken",sID)
        
        
        // Post notification after user successfully sign in
        NotificationCenter.default.post(name: .signInGoogleCompleted, object: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
                 withError error: Error!)
    {
        print("user has disconneted")
    }
    
    //MARK:- APPLE AUTHETICATION
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            print("User id is \(userIdentifier) \n Full Name is \(String(describing: fullName)) \n Email id is \(String(describing: email))")
            
            
            guard let authorizationCode = appleIDCredential.authorizationCode,
                  let authCode = String(data: authorizationCode, encoding: .utf8) else {
                print("Problem with the authorizationCode")
                return
            }
            
            
            if let authorizationCode = appleIDCredential.authorizationCode,
               let identifyToken = appleIDCredential.identityToken {
                print("authSIWAtoken: ",identifyToken.base64EncodedString())
            }
            
        }
     
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print("\(error.localizedDescription)")
    }
    //MARK:- FACEBOOK AUTHETICATION
    
    func loginfb()
    {
        
        let manage = LoginManager()
        manage.logIn(permissions: [.publicProfile,.email,.userGender,.userBirthday], viewController: self) { (result) in
            switch result{
            case.cancelled :
                print("User cancelled login process")
                break
            case.failed(let error):
                print("login failed with error = \(error.localizedDescription)")
                break
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("access token == \(accessToken.tokenString)")
                print("succesfull login")
                self.getUserProfile()
            }
        }
    }
   
    func getUserProfile() {
        
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,gender, birthday"], httpMethod: .get)) { (connection, response, error) in
            if let error = error {
                print("Error getting user info = \(error.localizedDescription)")
            } else {
                guard let userInfo = response as? Dictionary<String,Any> else {
                    return
                }
                
                if let userID = userInfo["id"] as? String {
                    print("fb id == \(userID)")
                    
                }
               
                if let userfirstname = userInfo["first_name"] as? String {
                    print("first name == \(userfirstname)")
                    
                }
                if let usersecondname = userInfo["last_name"] as? String {
                    print("last name == \(usersecondname)")
                    
                }
                if let email = userInfo["email"] as? String {
                    print("email == \(email)")
                    
                }else{
                    print("your email is not attached with facebook account")
                    return
                }
                
            }
        }
        connection.start()
    }
    
    
    
    
}

