//
//  SignUpViewController.swift
//  Yummy-Project
//
//  Created by WASIQ-MACBOOK on 29/01/2021.
//

import UIKit

class SignUpViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    
    
    @IBOutlet weak var btnEye: UIButton!
    
    //MARK:- VARIABLE DECLARATION
    
    var isSecure = true
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        if  AppUtility?.getObject(forKey: "DarKMode") == "ON"{
            overrideUserInterfaceStyle = .dark
            UIApplication.shared.statusBarStyle = .lightContent
            setNeedsStatusBarAppearanceUpdate()
            
        }else{
            overrideUserInterfaceStyle = .light
            UIApplication.shared.statusBarStyle = .darkContent
            setNeedsStatusBarAppearanceUpdate()
            
        }
    }
    
    
    //MARK:- FUNCTION
       
    
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        
        guard AppUtility?.isEmail(tfEmail.text!) == true else{
           AppUtility?.displayAlert(title: "Invalid Email", messageText: "Email is not Valid", delegate: self)
           return
           
        }
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "PhoneNumberViewController")as! PhoneNumberViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func eyeButtonPressed(_ sender: UIButton) {
        
        
        if self.isSecure == true {
            self.tfPassword.isSecureTextEntry =  false
            self.btnEye.setImage(UIImage(named: "openeye"), for: .normal)
            self.isSecure =  false
        }else{
            self.tfPassword.isSecureTextEntry =  true
            self.btnEye.setImage(UIImage(named: "eye"), for: .normal)
            self.isSecure =  true
        }
    }
    
    @IBAction func countrySelectButtonPressed(_ sender: UIButton) {
    }
}
