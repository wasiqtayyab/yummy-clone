//
//  DOBViewController.swift
//  Yummy-Project
//
//  Created by WASIQ-MACBOOK on 29/01/2021.
//

import UIKit

class DOBViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    
    //MARK:- VARIABLE DECLARATION
    var SelectedDOB = ""
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    
   
    
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        if  AppUtility?.getObject(forKey: "DarKMode") == "ON"{
            overrideUserInterfaceStyle = .dark
            UIApplication.shared.statusBarStyle = .lightContent
            setNeedsStatusBarAppearanceUpdate()
            
        }else{
            overrideUserInterfaceStyle = .light
            UIApplication.shared.statusBarStyle = .darkContent
            setNeedsStatusBarAppearanceUpdate()
            
        }
    }
    
    //MARK:- FUNCTION
    
    func setup(){
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
            
        }
    }
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        
        if SelectedDOB == "" {
            
            AppUtility?.displayAlert(title: NSLocalizedString("Date of Birth", comment: ""), messageText: NSLocalizedString("Date of Birth invalid", comment: ""), delegate: self)
            return
            
        }
        let age = AppUtility!.getAgeFromDOF(date:SelectedDOB)
        
         if age.0 < 15 {
         AppUtility?.displayAlert(title: NSLocalizedString("Date of Birth", comment: "") , messageText: "Your age must be greater than 15 years", delegate: self)
         return
         }
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddUserNameViewController")as! AddUserNameViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func datePickerChanged(sender: UIDatePicker) {
    
        print("print \(sender.date)")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let somedateString = dateFormatter.string(from: sender.date)
        self.SelectedDOB = somedateString
        print(somedateString)
      
    }
    
}
