//
//  AddUserNameViewController.swift
//  Yummy-Project
//
//  Created by WASIQ-MACBOOK on 29/01/2021.
//

import UIKit

class AddUserNameViewController: UIViewController {
    
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfUsername: UITextField!
    
    
    
    
    //MARK:- VARIABLE DECLARATION
    
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    //MARK:- VIEW WILL APPEAR
    
    
    override func viewWillAppear(_ animated: Bool) {
        if  AppUtility?.getObject(forKey: "DarKMode") == "ON"{
            overrideUserInterfaceStyle = .dark
            UIApplication.shared.statusBarStyle = .lightContent
            setNeedsStatusBarAppearanceUpdate()
            
        }else{
            overrideUserInterfaceStyle = .light
            UIApplication.shared.statusBarStyle = .darkContent
            setNeedsStatusBarAppearanceUpdate()
            
        }
    }
    
    //MARK:- FUNCTION
    
    
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        
        if AppUtility!.isEmpty(self.tfUsername.text!){
           
            AppUtility?.displayAlert(title: NSLocalizedString("Username", comment: ""), messageText: NSLocalizedString("Username Invalid", comment: ""), delegate: self)
            
            return
        }
        if AppUtility!.validateUsername(str: tfUsername.text!){
            print("usernamw not valid")
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
