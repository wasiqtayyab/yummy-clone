//
//  PhoneNumberViewController.swift
//  Yummy-Project
//
//  Created by WASIQ-MACBOOK on 29/01/2021.
//

import UIKit
import PhoneNumberKit

class PhoneNumberViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfMobileNumber: PhoneNumberTextField!
    
    @IBOutlet weak var tfCountryCode: UITextField!
    
    //MARK:- VARIABLE DECLARATION
    
    

    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        if  AppUtility?.getObject(forKey: "DarKMode") == "ON"{
            overrideUserInterfaceStyle = .dark
            UIApplication.shared.statusBarStyle = .lightContent
            setNeedsStatusBarAppearanceUpdate()
            
        }else{
            overrideUserInterfaceStyle = .light
            UIApplication.shared.statusBarStyle = .darkContent
            setNeedsStatusBarAppearanceUpdate()
            
        }
    }
    
    //MARK:- FUNCTION
    
    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func continueButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PhoneOTPViewController")as! PhoneOTPViewController
        vc.strPhoneNumber = self.tfMobileNumber.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countryListButtonPressed(_ sender: UIButton) {
    }
    
}
